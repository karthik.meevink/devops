#Open a command prompt in admin mode
#then run these commands in that command prompt
#bash
cd ~
cd ~/vagrant/centos
vagrant destroy -f
cd ~/vagrant/ubuntu
vagrant destroy -f
cd ~/vagrant/bento
vagrant destroy -f
cd ~
mkdir ~/devops
mkdir -p ~/devops/iso
mkdir -p ~/devops/provision ~/devops/release ~/devops/coding
mkdir -p ~/devops/release/git ~/devops/release/gitlab ~/devops/release/jenkins ~/devops/release/tomcat ~/devops/release/nagios
mkdir -p ~/devops/coding/puppet ~/devops/coding/chef ~/devops/coding/ansible
mkdir -p ~/devops/coding/puppet/puppetserver ~/devops/coding/puppet/puppetnode
mkdir -p ~/devops/coding/chef/chefserver ~/devops/coding/chef/chefworkstation ~/devops/coding/chef/chefnode
mkdir -p ~/devops/provision/vagrant ~/devops/provision/docker ~/devops/provision/kubernetes ~/devops/provision/terraform
mkdir -p ~/devops/provision/vagrant/centos ~/devops/provision/vagrant/ubuntu ~/devops/provision/vagrant/bento
cd ~/devops/provision/vagrant/centos
vagrant init geerlingguy/centos8
vagrant up && vagrant halt
cd ~/devops/provision/vagrant/bento
vagrant init bento/ubuntu-20.04
vagrant up && vagrant halt
cd ~/devops/provision/vagrant/ubuntu
vagrant init ubuntu/xenial64
vagrant up && vagrant halt
cd ~/devops/iso
wget https://sourceforge.net/projects/bodhilinux/files/6.0.0/bodhi-6.0.0-64.iso/download
echo "Lab Setup Complete"
